function wordFrequency(str) {
  let stringArr = str.split(" ");
  let stringObj = stringArr.reduce(function(obj, word) {
    if (obj.hasOwnProperty(word)) {
      obj[word]++;
    } else {
      obj[word] = 1;
    }
    return obj;
  }, {});
  console.log(stringObj);
}

wordFrequency("I scream you scream we all scream for ice cream");
